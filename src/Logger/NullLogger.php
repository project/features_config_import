<?php

namespace Drupal\features_config_import\Logger;

/**
 * Class NullLogger.
 *
 * Null logger class with success function.
 *
 * @package Drupal\features_config_import\Logger
 */
class NullLogger extends \Psr\Log\NullLogger {

  /**
   * Empty.
   */
  public function success() {

  }

}
