<?php

namespace Drupal\features_config_import\Plugin\FeaturesAssignment;

use Drupal\features\FeaturesAssignmentMethodBase;
use Drupal\features_config_import\Plugin\ConfigProvider\ConfigProviderFeaturesConfigImport;

/**
 * Class FeaturesAssignmentConfigImportType.
 *
 * This class that export the config to config_import folder
 * so that install gets handled by feature_config_import.
 *
 * @Plugin(
 *   id = "feature_config_import",
 *   weight = 0,
 *   name = @Translation("Features config import"),
 *   description = @Translation("Enable support for features config import"),
 * )
 */
class FeaturesAssignmentConfigImportType extends FeaturesAssignmentMethodBase {

  /**
   * {@inheritdoc}
   */
  public function assignPackages($force = FALSE) {
    $config_collection = $this->featuresManager->getConfigCollection();

    foreach ($config_collection as &$item) {
      $item->setSubdirectory(ConfigProviderFeaturesConfigImport::ID);
    }
    // Clean up the $item pass by reference.
    unset($item);
  }

}
