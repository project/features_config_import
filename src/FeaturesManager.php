<?php

namespace Drupal\features_config_import;

use Drupal\config_update\ConfigRevertInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\features\FeaturesManager as FeaturesManagerOriginal;
use Drupal\features_config_import\Plugin\ConfigProvider\ConfigProviderFeaturesConfigImport;

/**
 * {@inheritdoc}
 */
class FeaturesManager extends FeaturesManagerOriginal {

  /**
   * {@inheritdoc}
   */
  public function __construct($root, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, StorageInterface $config_storage, ConfigManagerInterface $config_manager, ModuleHandlerInterface $module_handler, ConfigRevertInterface $config_reverter, ModuleExtensionList $module_extension_list) {
    parent::__construct($root, $entity_type_manager, $config_factory, $config_storage, $config_manager, $module_handler, $config_reverter, $module_extension_list);
    $this->extensionStorages->addStorage(ConfigProviderFeaturesConfigImport::ID);
  }

}
