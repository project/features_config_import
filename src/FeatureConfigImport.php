<?php

namespace Drupal\features_config_import;

use Drupal\config\StorageReplaceDataWrapper;
use Drupal\config_provider\Plugin\ConfigCollectorInterface;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManager;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\features\Entity\FeaturesBundle;
use Drupal\features\FeaturesBundleInterface;
use Drupal\features\FeaturesManagerInterface;
use Drupal\features_config_import\Logger\NullLogger;
use Drupal\features_config_import\Plugin\ConfigProvider\ConfigProviderFeaturesConfigImport;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class FeatureConfigImport.
 *
 * Service class for feature config import.
 *
 * @package Drupal\features_config_import
 */
class FeatureConfigImport implements LoggerAwareInterface {

  use StringTranslationTrait;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Features manager.
   *
   * @var \Drupal\features\FeaturesManagerInterface
   */
  private $featureManager;

  /**
   * Config export commands.
   *
   * @var \Drupal\config_provider\Plugin\ConfigCollectorInterface
   */
  private $configProvider;

  /**
   * Config export commands.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Config import commands.
   *
   * @var \Drupal\config_provider\Plugin\ConfigProviderInterface
   */
  private $configCollector;

  /**
   * Config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  private $configManager;

  /**
   * Typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManager
   */
  private $typedConfigManager;

  /**
   * Lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  private $lockBackend;

  /**
   * Theme Handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  private $moduleInstaller;

  /**
   * Module extension.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleExtensionList;

  use LoggerAwareTrait;

  /**
   * FeatureConfigImport constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   * @param \Drupal\features\FeaturesManagerInterface $featuresManager
   *   Feature manager.
   * @param \Drupal\Core\Config\StorageInterface $configProvider
   *   Config provider.
   * @param \Drupal\config_provider\Plugin\ConfigCollectorInterface $configCollector
   *   Config collector.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Config\ConfigManagerInterface $configManager
   *   Config manager.
   * @param \Drupal\Core\Config\TypedConfigManager $typedConfigManager
   *   Typed config manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lockBackend
   *   Lock backend.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   Theme handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller
   *   Module installer.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   Module extension list.
   */
  public function __construct(FileSystemInterface $fileSystem, FeaturesManagerInterface $featuresManager, StorageInterface $configProvider, ConfigCollectorInterface $configCollector, ModuleHandlerInterface $moduleHandler, ConfigManagerInterface $configManager, TypedConfigManager $typedConfigManager, LockBackendInterface $lockBackend, ThemeHandlerInterface $themeHandler, EventDispatcherInterface $eventDispatcher, ModuleInstallerInterface $moduleInstaller, ModuleExtensionList $moduleExtensionList) {
    $this->fileSystem = $fileSystem;
    $this->featureManager = $featuresManager;
    $this->configProvider = $configProvider;
    $this->configCollector = $configCollector;
    $this->moduleHandler = $moduleHandler;
    $this->configManager = $configManager;
    $this->typedConfigManager = $typedConfigManager;
    $this->lockBackend = $lockBackend;
    $this->themeHandler = $themeHandler;
    $this->eventDispatcher = $eventDispatcher;
    $this->moduleInstaller = $moduleInstaller;
    $this->moduleExtensionList = $moduleExtensionList;
    $this->setLogger(new NullLogger());
  }

  /**
   * Return feature modules.
   *
   * @param \Drupal\features\FeaturesBundleInterface $featureBundle
   *   Feature bundle if exists.
   * @param array $ignoreModules
   *   Ignore modules.
   *
   * @return array
   *   List of feature modules.
   */
  public function getEnabledFeatureModules(FeaturesBundleInterface $featureBundle = NULL, array $ignoreModules = []) {
    $returnModules = [];
    $ignoreModules = array_merge($ignoreModules, [
      'webform_demo_application_evaluation',
      'webform_demo',
      'webform_demo_event_registration',
      'webform_scheduled_email_test',
    ]);

    $modules = $this->featureManager->getFeaturesModules($featureBundle);
    foreach ($modules as $module) {
      if (in_array($module->getName(), $ignoreModules) || !$this->moduleHandler->moduleExists($module->getName())) {
        continue;
      }
      $returnModules[$module->getName()] = $module;
    }

    $this->logger->notice('Found following enabled features: ' . implode(array_keys($modules)));

    return $returnModules;
  }

  /**
   * Copy feature config to specific folder.
   *
   * @param string $destination_dir
   *   Destination.
   * @param \Drupal\features\FeaturesBundleInterface|null $featureBundle
   *   Feature bundle.
   * @param string[] $ignoreModules
   *   Ignore modules.
   */
  public function copyFeatureConfigs($destination_dir = NULL, FeaturesBundleInterface $featureBundle = NULL, array $ignoreModules = []) {
    $modules = $this->getEnabledFeatureModules($featureBundle, $ignoreModules);
    $this->logger->notice('Start copying all feature files');
    foreach ($modules as $module) {
      if (in_array($module->getName(), $ignoreModules)) {
        continue;
      }

      $files = $this->fileSystem->scanDirectory($module->getPath() . '/config/import', '/.*\.yml$/');
      foreach ($files as $file) {
        $this->fileSystem->copy($file->uri, $destination_dir, FileSystemInterface::EXISTS_REPLACE);
      }
    }
  }

  /**
   * Gets the existing collections from configuration storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The configuration storage engine.
   *
   * @return string[]
   *   An array of existing collection names.
   */
  private function getAllCollectionNames(StorageInterface $storage) {
    $collection_names = $storage->getAllCollectionNames();
    $collection_names[] = StorageInterface::DEFAULT_COLLECTION;
    $collection_names = array_unique($collection_names);
    return $collection_names;
  }

  /**
   * Import feature configs.
   *
   * @param string|null $featureBundle
   *   Feature bundle name.
   * @param array $ignoreModules
   *   Ignore modules.
   * @param bool $dryRun
   *   Dry run.
   *
   * @return \Drupal\Core\Config\ConfigImporter
   *   Return config importer.
   */
  public function importFeatureConfigs(string $featureBundle = NULL, array $ignoreModules = [], $dryRun = FALSE) {
    $featureBundleEntity = NULL;

    if (!empty($featureBundle)) {
      $featureBundleEntity = FeaturesBundle::load($featureBundle);
    }

    $configProviderFeatureConfigImport = $this->configCollector->getConfigProviders()[ConfigProviderFeaturesConfigImport::ID];
    $configProviderFeatureConfigImport->addInstallableConfig($this->getEnabledFeatureModules($featureBundleEntity, $ignoreModules));

    $replacement_storage = new StorageReplaceDataWrapper($this->featureManager->getActiveStorage());
    foreach ($this->configProvider->listAll() as $configKey) {
      $replacement_storage->replaceData($configKey, $this->configProvider->read($configKey));
    }

    $storageComparer = new StorageComparer(
      $replacement_storage,
      $this->featureManager->getActiveStorage()
    );

    $configImport = new ConfigImporter(
      $storageComparer->createChangelist(),
      $this->eventDispatcher,
      $this->configManager,
      $this->lockBackend,
      $this->typedConfigManager,
      $this->moduleHandler,
      $this->moduleInstaller,
      $this->themeHandler,
      $this->getStringTranslation(),
      $this->moduleExtensionList
    );

    if (!$dryRun) {
      $this->logger->notice("Start importing enabled features configuration");
      $configImport->import();
    }

    return $configImport;
  }

}
