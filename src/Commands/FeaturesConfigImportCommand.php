<?php

namespace Drupal\features_config_import\Commands;

use Drupal\Core\Site\Settings;
use Drupal\features_config_import\FeatureConfigImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This is a literal copy of the example Symfony Console command
 * from the documentation.
 *
 * See:
 * http://symfony.com/doc/2.7/components/console/introduction.html#creating-a-basic-command
 */
class FeaturesConfigImportCommand extends Command {

  protected $featureConfigImport;

  public function __construct(string $name = NULL, FeatureConfigImport $featureConfigImport) {
    parent::__construct($name);
    $this->featureConfigImport = $featureConfigImport;
  }


  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('features-config-import:run')
      ->setDescription('Run features configuration import')
      ->addOption(
        'bundle',
        NULL,
        InputOption::VALUE_OPTIONAL,
        'If set, the task will only use a specific bundle'
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $bundleEntity = NULL;
    // Disable interactive.
    $input->setInteractive(FALSE);
    // Set logger.
    $this->featureConfigImport->setLogger(new ConsoleLogger($output));

    $exportCommand = $this->getApplication()->find('config:export');

    $arguments = [
      'command' => ['config:export'],
    ];

    $configExportInput = new ArrayInput($arguments);
    $configExportInput->setInteractive(0);
    $status = $exportCommand->run($configExportInput, $output);
    if ($status != 0) {
      return $status;
    }

    $destination_dir = Settings::get('config_sync_directory');

    $this->featureConfigImport->copyFeatureConfigs($destination_dir, $input->getOption('bundle'));

    $importCommand = $this->getApplication()->find('config:import');

    $arguments = [
      'command' => 'config:import',
    ];

    $configImportInput = new ArrayInput($arguments);
    $configImportInput->setInteractive(0);
    return $importCommand->run($configImportInput, $output);
  }

}
